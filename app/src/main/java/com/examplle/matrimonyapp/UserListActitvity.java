package com.examplle.matrimonyapp;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.examplle.matrimonyapp.adapter.UserListAdapter;
import com.examplle.matrimonyapp.database.TblUser;
import com.examplle.matrimonyapp.model.UserModel;

import java.util.ArrayList;

public class UserListActitvity extends BaseActivity {

    RecyclerView rcvUserList;

    ArrayList<UserModel> userlist = new ArrayList<>();
    UserListAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_list);
        initViewReference();
        setAdapter();
    }

    void setAdapter(){
        rcvUserList.setLayoutManager(new GridLayoutManager(this,1));
        userlist.addAll(new TblUser(this).getUserList());
        adapter = new UserListAdapter(this,userlist,null);
        rcvUserList.setAdapter(adapter);
    }

    void initViewReference(){
        rcvUserList = findViewById(R.id.rcvUserList);
    }
}
