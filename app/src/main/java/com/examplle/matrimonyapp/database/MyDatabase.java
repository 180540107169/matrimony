package com.examplle.matrimonyapp.database;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class MyDatabase extends SQLiteAssetHelper {

    public static  final String DATABASE_NAME = "Matrimony.db";
    public  static  final  int VERSION = 1;


    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }
}
