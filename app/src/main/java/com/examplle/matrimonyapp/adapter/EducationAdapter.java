package com.examplle.matrimonyapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.examplle.matrimonyapp.R;
import com.examplle.matrimonyapp.model.EducationModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EducationAdapter extends BaseAdapter {
    Context context;
    ArrayList<EducationModel> educationlist;

    public EducationAdapter(Context context, ArrayList<EducationModel> educationlist) {
        this.context = context;
        this.educationlist = educationlist;
    }

    @Override
    public int getCount() {
        return educationlist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewgroup) {
          View v = view;
          EducationAdapter.ViewHolder viewHolder;
          if(v==null){
              v = LayoutInflater.from(context).inflate(R.layout.view_row_text,null);
              viewHolder = new ViewHolder(v);
              v.setTag(viewHolder);
          }else{
              viewHolder = (EducationAdapter.ViewHolder) v.getTag();
          }
          viewHolder.tvSpinnerList.setText(educationlist.get(i).getEducationName());

          if(i==0){
              viewHolder.tvSpinnerList.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
          }else{
              viewHolder.tvSpinnerList.setTextColor(ContextCompat.getColor(context,R.color.colorBlack));
          }
          return v;
    }

    @Override
    public boolean isEnabled(int i) {
        if (i == 0) {
            return false;
        } else {
            return true;
        }
    }

    static
    class ViewHolder {
        @BindView(R.id.tvSpinnerList)
        TextView tvSpinnerList;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
