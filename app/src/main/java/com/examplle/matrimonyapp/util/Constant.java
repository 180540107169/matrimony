package com.examplle.matrimonyapp.util;

public class Constant {

    public static final int MALE = 1;
    public static final int FEMALE = 2;

    public static final String GENDER = "Gender";

    public  static  final  String CRICKET = "Cricket";
    public  static  final  String FOOTBALL = "Football";
    public  static  final  String BASKETBALL = "BasketBall";
    public  static  final  String HOCKEY = "Hockey";

    public static final String USER_OBJECT = "UserObject";
}
