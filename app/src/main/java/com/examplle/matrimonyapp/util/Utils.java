package com.examplle.matrimonyapp.util;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Utils {

    public  static Date getDatefromString(String dateToConvert){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(dateToConvert);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public  static Date getDatefromStringUpdated(String dateToConvert){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;
        try {
            date = format.parse(dateToConvert);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public  static String getDateToDisplay(String dateToConvert){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String convertedDate = "";
        try {
            Date date = format.parse(dateToConvert);
            convertedDate = dateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public  static  String getFormatedDateToInserted(String dateToConvert){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat converted_date_format = new SimpleDateFormat("yyyy-MM-dd");
        String convertedDate = "";
        try {
            Date date = dateFormat.parse(dateToConvert);
            convertedDate = converted_date_format.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public static String getAge(int year, int month, int day){
        Calendar Dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        Log.d("ageeeee", "getAge: "+year+month+day);
        Dob.set(year, month, day);
        int age = today.get(Calendar.YEAR) - Dob.get(Calendar.YEAR);
        Log.d("aaa", "getAge: "+today.get(Calendar.YEAR));
        Log.d("bbbaa", "getAge: "+Dob.get(Calendar.YEAR));
        if (today.get(Calendar.DAY_OF_YEAR) < Dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageS;
    }
}
