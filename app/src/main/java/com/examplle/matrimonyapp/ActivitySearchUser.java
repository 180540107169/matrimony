package com.examplle.matrimonyapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.examplle.matrimonyapp.adapter.UserListAdapter;
import com.examplle.matrimonyapp.database.TblUser;
import com.examplle.matrimonyapp.model.UserModel;
import com.examplle.matrimonyapp.util.Constant;
import com.examplle.matrimonyapp.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivitySearchUser extends BaseActivity {
    @BindView(R.id.etActSearch)
    EditText etActSearch;
    @BindView(R.id.rcvUsers)
    RecyclerView rcvUsers;
    @BindView(R.id.tvNoSearchDataFound)
    TextView tvNoSearchDataFound;


    ArrayList<UserModel> userlist = new ArrayList<>();
    ArrayList<UserModel> tempUserList = new ArrayList<>();
    UserListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_candidate);
        ButterKnife.bind(this);
        setAdapter();
        setSeachUser();
        checkAndVisibleView();
    }

    void resetAdapter() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }


    void setSeachUser() {
        etActSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tempUserList.clear();
                if (s.toString().length() > 0) {
                    for (int i = 0; i < userlist.size(); i++) {

                        String firstName = userlist.get(i).getFirstName().toLowerCase() + " " +
                                userlist.get(i).getLastName().toLowerCase();

                        String fatherName = userlist.get(i).getMiddleName().toLowerCase() + " " +
                                userlist.get(i).getLastName().toLowerCase();

                        String fullName = userlist.get(i).getFirstName().toLowerCase() + " " +
                                userlist.get(i).getMiddleName().toLowerCase() + " " +
                                userlist.get(i).getLastName().toLowerCase();

                        String inverseFirstName = userlist.get(i).getLastName().toLowerCase() + " " +
                                userlist.get(i).getFirstName().toLowerCase();

                        String inverseFatherName = userlist.get(i).getLastName().toLowerCase() + " " +
                                userlist.get(i).getMiddleName().toLowerCase();

                        String inversefullName = userlist.get(i).getLastName().toLowerCase() + " " +
                                userlist.get(i).getFirstName().toLowerCase() + " " +
                                userlist.get(i).getMiddleName().toLowerCase();
                        String dateParts[] = userlist.get(i).getDob().split("/");
                        String age = Utils.getAge(Integer.parseInt(dateParts[2]),Integer.parseInt(dateParts[1]),Integer.parseInt(dateParts[0]));

                        if (userlist.get(i).getFirstName().toLowerCase().contains(s.toString().toLowerCase())
                                || userlist.get(i).getMiddleName().toLowerCase().contains(s.toString().toLowerCase())
                                || userlist.get(i).getLastName().toLowerCase().contains(s.toString().toLowerCase())
                                || userlist.get(i).getMobileNumber().toLowerCase().contains(s.toString().toLowerCase())
                                || userlist.get(i).getEmail().toLowerCase().contains(s.toString().toLowerCase())
                                || userlist.get(i).getCity().toLowerCase().contains(s.toString().toLowerCase())
                                || userlist.get(i).getLanguage().toLowerCase().contains(s.toString().toLowerCase())
                                || userlist.get(i).getEducation().toLowerCase().contains(s.toString().toLowerCase())
                                || firstName.contains(s.toString().toLowerCase())
                                || fatherName.contains(s.toString().toLowerCase())
                                || fullName.contains(s.toString().toLowerCase())
                                || inverseFirstName.contains(s.toString().toLowerCase())
                                || inverseFatherName.contains(s.toString().toLowerCase())
                                || inversefullName.contains(s.toString().toLowerCase())
                                || age.contains(s.toString().toLowerCase())) {
                            tempUserList.add(userlist.get(i));
                            rcvUsers.setVisibility(View.VISIBLE);
                            tvNoSearchDataFound.setVisibility(View.GONE);
                        }
                    }
                }else {
                    tempUserList.addAll(userlist);
                    rcvUsers.setVisibility(View.VISIBLE);
                    tvNoSearchDataFound.setVisibility(View.GONE);
                }
                if (tempUserList.size() == 0) {
                    rcvUsers.setVisibility(View.GONE);
                    tvNoSearchDataFound.setVisibility(View.VISIBLE);
                }
                resetAdapter();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    void setAdapter() {
        rcvUsers.setLayoutManager(new GridLayoutManager(this, 1));
        userlist.addAll(new TblUser(this).getUserList());
        tempUserList.addAll(userlist);
        adapter = new UserListAdapter(this, tempUserList, new UserListAdapter.OnViewClickListener() {
            @Override
            public void OnDeleteClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void OnItemClicked(int position) {
                Intent intent = new Intent(ActivitySearchUser.this,UserListRegistration.class);
                intent.putExtra(Constant.USER_OBJECT,userlist.get(position));
                startActivity(intent);
            }

            @Override
            public void OnFavouriteClick(int position) {
                int lastUpdatedFavouriteStatus = new TblUser(ActivitySearchUser.this).updateFavouriteStatus(userlist.get(position).getIsFavourite() == 0 ? 1 : 0,userlist.get(position).getUserId());
                if(lastUpdatedFavouriteStatus > 0 ){
                    userlist.get(position).setIsFavourite(userlist.get(position).getIsFavourite() == 0 ? 1 : 0);
                    adapter.notifyItemChanged(position);
                }
            }
        });
        rcvUsers.setAdapter(adapter);
    }

    void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(ActivitySearchUser.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are You Sure Want To Delete This User?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                (dialog, which) -> {
                    int deletedUserId = new TblUser(ActivitySearchUser.this).deleteUserById(userlist.get(position).getUserId());
                    if (deletedUserId > 0) {
                        Toast.makeText(ActivitySearchUser.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        tempUserList.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userlist.size());
                        checkAndVisibleView();
                    } else {
                        Toast.makeText(ActivitySearchUser.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }

    void checkAndVisibleView() {
        if (tempUserList.size() > 0) {
            tvNoSearchDataFound.setVisibility(View.GONE);
            rcvUsers.setVisibility(View.VISIBLE);
        } else {
            tvNoSearchDataFound.setVisibility(View.VISIBLE);
            rcvUsers.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ActivitySearchUser.this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }
}
