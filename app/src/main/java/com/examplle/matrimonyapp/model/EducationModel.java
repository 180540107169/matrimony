package com.examplle.matrimonyapp.model;

import java.io.Serializable;

public class EducationModel implements Serializable {

    int EducationId;
    String EducationName;

    @Override
    public String toString() {
        return "EducationModel{" +
                "EducationId=" + EducationId +
                ", EducationName='" + EducationName + '\'' +
                '}';
    }

    public int getEducationId() {
        return EducationId;
    }

    public void setEducationId(int educationId) {
        EducationId = educationId;
    }

    public String getEducationName() {
        return EducationName;
    }

    public void setEducationName(String educationName) {
        EducationName = educationName;
    }
}
